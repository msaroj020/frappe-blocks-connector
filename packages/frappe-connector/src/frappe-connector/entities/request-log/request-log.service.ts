import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { RequestLog } from './request-log.entity';

@Injectable()
export class RequestLogService {
  constructor(
    @InjectRepository(RequestLog)
    private readonly requestLogRepository: MongoRepository<RequestLog>,
  ) {}

  async save(params) {
    return await this.requestLogRepository.save(params);
  }

  async find(): Promise<RequestLog[]> {
    return await this.requestLogRepository.find();
  }

  async findOne(params): Promise<RequestLog> {
    return await this.requestLogRepository.findOne(params);
  }

  async update(query, params) {
    return await this.requestLogRepository.update(query, params);
  }

  async count() {
    return await this.requestLogRepository.count();
  }

  async paginate(skip: number, take: number) {
    return await this.requestLogRepository.find({ skip, take });
  }

  async deleteMany(params) {
    return await this.requestLogRepository.deleteMany(params);
  }
}
