import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { FrappeClientAddedEvent } from './frappe-client-added.event';

@EventsHandler(FrappeClientAddedEvent)
export class FrappeClientAddedHandler
  implements IEventHandler<FrappeClientAddedEvent> {
  handle(event: FrappeClientAddedEvent) {
    const { client: provider } = event;
    provider
      .save()
      .then(success => {})
      .catch(error => {});
  }
}
