import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListFrappeClientQuery } from './list-frappe-client.query';
import { FrappeClientAggregateService } from '../../aggregates/frappe-client-aggregate/frappe-client-aggregate.service';

@QueryHandler(ListFrappeClientQuery)
export class ListFrappeClientHandler
  implements IQueryHandler<ListFrappeClientQuery> {
  constructor(private manager: FrappeClientAggregateService) {}

  async execute(query: ListFrappeClientQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.list(offset, limit, search, sort);
  }
}
