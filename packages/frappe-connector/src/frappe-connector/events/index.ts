import { FrappeClientAddedHandler } from './frappe-client-added/frappe-client-added.handler';
import { FrappeClientRemovedHandler } from './frappe-client-removed/frappe-client-removed.handler';
import { FrappeClientUpdatedHandler } from './frappe-client-updated/frappe-client-updated.handler';

export const FrappeEventHandlers = [
  FrappeClientAddedHandler,
  FrappeClientRemovedHandler,
  FrappeClientUpdatedHandler,
];
