import { ListFrappeClientHandler } from './list-frappe-client/list-frappe-client.handler';
import { RetrieveFrappeClientHandler } from './retrieve-frappe-client/retrieve-frappe-client.handler';
import { VerifyClientConnectionHandler } from './verify-client-connection/verify-client-connection.handler';

export const FrappeQueryHandlers = [
  ListFrappeClientHandler,
  RetrieveFrappeClientHandler,
  VerifyClientConnectionHandler,
];
