import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { RetrieveFrappeClientQuery } from './retrieve-frappe-client.query';
import { FrappeClientAggregateService } from '../../aggregates/frappe-client-aggregate/frappe-client-aggregate.service';

@QueryHandler(RetrieveFrappeClientQuery)
export class RetrieveFrappeClientHandler
  implements IQueryHandler<RetrieveFrappeClientQuery> {
  constructor(private manager: FrappeClientAggregateService) {}

  async execute(query: RetrieveFrappeClientQuery) {
    const { uuid } = query;
    return await this.manager.retrieveProvider(uuid);
  }
}
